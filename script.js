const movie = {
    movieTitle: "Harry Potter et la Chambre des secrets",
    yearOfCompletion: 2002,
    director: "Chris Columbus",
    mainActors: "Daniel Radcliffe , Emma Watson",
    isAFavoriteMovie: true,
    ratingOfFilm: 3.9,
    duration: 0,
    setDuration: function (numberOfHours, numberOfMinutes) {
        // Je stock dans duration les heures que je multiplies par 60 + les minutes
        this.duration = numberOfHours * 60 + numberOfMinutes;
    }

};
movie.setDuration(2, 30); //  définira la durée du film à 2 heures et 30 minutes

console.log(movie);